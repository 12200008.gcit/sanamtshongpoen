import React, { useEffect, useReducer, useState } from "react";
import { Link, Navigate, Outlet, Route, Routes } from "react-router-dom";
import "../App.css";
import FarmerOrders from "./FarmerOrders";
import ProfileSettings from "./ProfileSettings";
import config from "./config";

const AdminDashboard = ({ setUser, setAdmin }) => {
  const [name, setName] = useState("");
  const farmer_id = sessionStorage.getItem("user_id");
  async function getName() {
    try {
      const response = await fetch(`${config.API_URL}users/${farmer_id}/name`, {
        method: "GET",
      });

      const userNameRes = await response.json();
      setName(userNameRes);
    } catch (err) {
      console.error(err.message);
    }
  }

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    sessionStorage.removeItem("token");

    setAdmin(false);
    sessionStorage.setItem("admin", false);
    setUser(null);
    window.location.replace("https://sanamtshongpoen.vercel.app");
  };
  useEffect(() => {
    getName();
  }, []);

  return (
    <div class="container-fluid ">
      <div class="row flex-nowrap bg-light">
        <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-light shadow">
          <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
            <div class="fs-5 d-none d-sm-inline text-dark fs-4 fw-1">
              <a class="navbar-brand">
                <Link to="/admindashboard">
                  <img src={require("../assets/logo.png")} />
                </Link>
              </a>
            </div>
            <ul
              class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start"
              id="menu"
            >
              <li>
                <Link to="dashboard" className="text-decoration-none">
                  <a class="nav-link px-0 align-middle">
                    <i class="fs-4 text-success bi-speedometer2 "></i>
                    <span class="ms-1 text-muted d-none d-sm-inline">
                      Dashboard
                    </span>{" "}
                  </a>
                </Link>
              </li>

              <li>
                <a
                  href="#submenu3"
                  data-bs-toggle="collapse"
                  class="nav-link px-0 align-middle"
                >
                  <i class="fs-4 text-success bi-grid"></i>{" "}
                  <span class="ms-1 text-muted d-none d-sm-inline">Update</span>{" "}
                </a>
                <ul
                  class="collapse nav flex-column ms-1"
                  id="submenu3"
                  data-bs-parent="#menu"
                >
                  <li class="w-100">
                    <Link to="stockupload" className="text-decoration-none">
                      <a
                        href="#"
                        class="nav-link px-0 text-decoration-none text-muted"
                      >
                        <i class="fs-6 text-success bi-upload"></i>{" "}
                        <span class="d-none d-sm-inline">Stock upload</span>
                      </a>
                    </Link>
                  </li>
                  <li class="w-100">
                    <Link to="stockupdate" className="text-decoration-none">
                      <a
                        href="#"
                        class="nav-link px-0 text-decoration-none text-muted"
                      >
                        <i class="fs-6 text-success bi-clipboard2-check"></i>{" "}
                        <span class="d-none d-sm-inline">Stock Update</span>
                      </a>
                    </Link>
                  </li>
                </ul>
              </li>

              <Link to="profilesettings" className="text-decoration-none">
                <li>
                  <a href="#" class="nav-link px-0 align-middle">
                    <i class="fs-4 text-success bi-people"></i>{" "}
                    <span class="ms-1 text-muted d-none d-sm-inline">
                      Profile Settings
                    </span>{" "}
                  </a>
                </li>
              </Link>
            </ul>
            <hr />
            <div class="dropdown pb-4">
              <a
                href="#"
                class="d-flex align-items-center text-success text-decoration-none dropdown-toggle"
                id="dropdownUser1"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <span class="d-none d-sm-inline mx-2">{name}</span>
              </a>
              <ul class="dropdown-menu dropdown-menu-white text-small shadow">
                <li>
                  <a class="dropdown-item" href="#">
                    <button
                      className="btn btn-danger"
                      onClick={(e) => logout(e)}
                    >
                      Sign out
                    </button>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        {/* content area */}
        <div class="col m-0 p-0 w-100 vh-100 border bg-red">
          <div
            class="overflow-auto scrollable-div"
            style={{ height: "100%", width: "100%" }}
          >
            <nav class="navbar navbar-light bg-light shadow my-2 rounded m-3">
              <div class="container-fluid mx-2">
                <a class="navbar-brand ms-auto" href="#">
                  SanamTshongpoen
                </a>
              </div>
            </nav>
            <div className="p-4">
              <Outlet></Outlet>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
