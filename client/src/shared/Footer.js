import React, { Fragment } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
const Footer = () => {
  return (
    <div class="container d-block border-top">
      <footer class="pt-5">
        <div class="row d-flex justify-content-between">
          <div class="d-flex justify-content-center align-items-center col-12 col-md-4 mb-3">
            <div class="navbar-brand">
              <img src={require("../assets/logo.png")} />
            </div>
          </div>
          <div class="col-6 col-md-2 mb-3 d-flex flex-column justify-content-center">
            <h5>Section</h5>
            <ul class="nav flex-column">
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  Wishlist
                </a>
              </li>
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  Cart
                </a>
              </li>
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  Track Order
                </a>
              </li>
            </ul>
          </div>

          <div class="col-6 col-md-2 mb-3 d-flex flex-column justify-content-center">
            <h5>Useful links</h5>
            <ul class="nav flex-column">
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  About us
                </a>
              </li>
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  Contact
                </a>
              </li>
              <li class="nav-item mb-3">
                <a href="#" class="nav-link p-0 text-muted">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-md-4 mb-3 d-flex flex-column justify-content-center">
            <ul class="nav flex-column">
              <li class="nav-item mb-3">
                <span class="nav-link p-0 text-dark">
                  <i class="fa-solid me-2 fa-location-dot"></i> Address: Thimphu
                  Bhutan
                </span>
              </li>
              <li class="nav-item mb-3">
                <span class="nav-link p-0 text-dark">
                  <i class="fa-solid me-2 fa-phone"></i> Call Us: 17330827
                </span>
              </li>
              <li class="nav-item mb-3">
                <span class="nav-link p-0 text-dark">
                  <i class="fa-solid me-2 fa-envelope"></i> Email:
                  sanamtshongpoen@contact.com
                </span>
              </li>
              <li class="nav-item mb-3">
                <span class="nav-link p-0 text-dark">
                  <i class="fa-solid me-2 fa-clock"></i> Work hours: 8:00 -
                  20:00, Sunday - Thursday
                </span>
              </li>
            </ul>
          </div>
        </div>

        <div class="d-flex flex-column flex-sm-row justify-content-between py-4 my-4 border-top">
          <p>&copy; 2023, All rights reserved.</p>
          <ul class="list-unstyled d-flex">
            <li class="ms-3">
              <a class="link-dark" href="#">
                <i class="fa-brands fa-xl fa-facebook"></i>
              </a>
            </li>
            <li class="ms-3">
              <a class="link-dark" href="#">
                <i class="fa-brands fa-xl fa-instagram"></i>
              </a>
            </li>
            <li class="ms-3">
              <a class="link-dark" href="#">
                <i class="fa-brands fa-xl fa-twitter"></i>
              </a>
            </li>
          </ul>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
