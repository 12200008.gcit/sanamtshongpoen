import React, { Fragment, useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import "./navbar.css";
import config from "../Admin/config";
const Navbar = ({ setAuth, setUser, setQuery }) => {
  const searchText = useRef();
  const selectedCategory = useRef();
  const [name, setName] = useState("");
  const [count, setCount] = useState(
    JSON.parse(sessionStorage.getItem("cart")) || []
  );
  async function getName() {
    try {
      const response = await fetch(`${config.API_URL}dashboard`, {
        method: "GET",
        headers: { token: localStorage.token },
      });

      const parseRes = await response.json();

      setName(parseRes.user_name);
    } catch (err) {
      console.error(err.message);
    }
  }
  // function setQuery(){
  //   fs
  // }
  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    sessionStorage.removeItem("token");

    setUser(null);
  };
  function handleSearch(event) {
    event.preventDefault();

    const searchTextFinal = searchText.current.value;
    const selectedCategoryFinal = selectedCategory.current.value;
    console.log(window.location.href);
    if (
      window.location.href !== "https://sanamtshongpoen.vercel.app/products"
    ) {
      window.location.replace("https://sanamtshongpoen.vercel.app/products");
    } else {
      setQuery(searchTextFinal, selectedCategoryFinal);
    }
  }

  useEffect(() => {
    getName();
  }, []);

  return (
    <nav
      class=" navbar navbar-expand-lg  navbar-light bg-light shadow-sm "
      // style={{ height: 90 }}
    >
      <div class="container">
        <div class="">
          <a class="navbar-brand">
            <Link to="/dashboard">
              <img src={require("../assets/logo.png")} />
            </Link>
          </a>
        </div>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <form
            class="navbar-form d-lg-flex w-lg-50 ms-lg-4"
            onSubmit={handleSearch}
          >
            <div class="input-group ">
              <div className="">
                <select
                  className="form-select"
                  id="dropdownMenu"
                  ref={selectedCategory}
                >
                  <option className="font-500" value="All">
                    All Category
                  </option>
                  <option className="font-500" value="Fruits">
                    Fruits
                  </option>
                  <option className="font-500" value="Vegetables">
                    Vegetables
                  </option>
                  <option className="font-500" value="Herbs and Spices">
                    Herbs and Spices
                  </option>
                  <option className="font-500" value="Meat and Poultry">
                    Meat and Poultry
                  </option>
                  <option className="font-500" value="Dairy Products">
                    Dairy Products
                  </option>
                </select>
              </div>
              <input
                ref={searchText}
                type="text"
                class="form-control font-500"
                placeholder="Search for items"
                aria-label="Search for items"
                aria-describedby="button-addon2"
              />
              <button class="btn btn-success" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </form>
          <ul class="navbar-nav ms-lg-auto  mb-lg-0">
            <li class="nav-item mx-lg-1">
              <a class="nav-link active">
                <Link
                  to="/wishlist"
                  class="nav-link active navigation-item d-lg-flex justify-content-lg-center align-items-lg-center"
                >
                  <i
                    class="fa-solid fa-heart me-2 fs-4"
                    style={{ color: "#198754" }}
                  ></i>
                  <span className="font-500 navbar-items-text">Wishlist</span>
                </Link>
              </a>
            </li>
            <li class="nav-item mx-lg-1">
              <a class="nav-link active">
                <Link
                  to="/cart"
                  className="nav-link active navigation-item d-lg-flex justify-content-lg-center align-items-lg-center"
                >
                  <div style={{ position: "relative" }}>
                    <i
                      className="fa-solid fa-cart-shopping me-lg-2 fs-4"
                      style={{ color: "#198754" }}
                    ></i>
                    <span
                      className="shadow-lg"
                      style={{
                        position: "absolute",
                        top: "-9px",
                        right: "-1px",
                        backgroundColor: "orange",
                        color: "white",

                        borderRadius: "50%",
                        padding: "8px",
                        width: "18px",
                        height: "18px",
                        fontSize: "12px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      {JSON.parse(sessionStorage.getItem("cart")) ||
                      [] === undefined
                        ? Object.keys(
                            JSON.parse(sessionStorage.getItem("cart")) || [][0]
                          ).length
                        : 0}
                    </span>
                  </div>
                  <span className="font-500 navbar-items-text">Mycart</span>
                </Link>
              </a>
            </li>

            <li class="nav-item  mx-lg-1 active dropdown  d-lg-flex justify-content-lg-center align-items-lg-center">
              <a
                class="nav-link dropdown-toggle"
                id="navbarDropdown"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                <img
                  src={require("../assets/userimg/user.png")}
                  class="rounded-circle me-lg-2"
                  width="30"
                  height="30"
                  alt="User Image"
                />
                <span className="font-500 navbar-items-text">
                  {name ? name.charAt(0).toUpperCase() : <sapn>Fname</sapn>}
                  {name ? name.substring(1) : <sapn> Lname</sapn>}
                </span>
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <Link to="/profile" className="text-decoration-none">
                    <button class="dropdown-item font-500">
                      Manage my account
                    </button>
                  </Link>
                </li>
                <li>
                  <Link to="/myorders" className="text-decoration-none">
                    <button class="dropdown-item font-500">My orders</button>
                  </Link>
                </li>
                <li>
                  <hr class="dropdown-divider" />
                </li>
                <li>
                  <button
                    class=" dropdown-item font-500 "
                    onClick={(e) => logout(e)}
                  >
                    Logout
                  </button>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
